# admin_project

#### 项目介绍
一套基于TP5和vue(D2-admin+elementui)的后台管理系统模板

#### 软件架构
一个非常简单的后台模板，适用于小微项目
ThinkPHP5
Vue
D2-admin
ElementUI


#### 安装教程

1. 克隆整个项目
2. 修改并启动PHP环境
3. 进入d2-admin-start-kit，npm install，npm run dev
 

#### 使用说明

1. 具备PHP能力
2. 具备Vue能力
3. 具备百度能力

#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)