import request from '@/plugin/axios'

export function AccountLogin (data) {
  return request({
    url: '/login/loginUser?XDEBUG_SESSION_START=12807',
    method: 'post',
    data
  })
}
