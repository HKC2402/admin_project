/*
Navicat MySQL Data Transfer

Source Server         : 本地
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : admin_project

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2018-10-30 14:19:46
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for com_admin
-- ----------------------------
DROP TABLE IF EXISTS `com_admin`;
CREATE TABLE `com_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `username` varchar(255) DEFAULT NULL COMMENT '用户名',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `level` smallint(255) DEFAULT NULL COMMENT '权限等级',
  `nickname` varchar(255) DEFAULT NULL COMMENT '昵称',
  `phone` varchar(255) DEFAULT NULL COMMENT '电话',
  `ip` varchar(255) DEFAULT NULL COMMENT '登录ip',
  `last_ip` varchar(255) DEFAULT NULL COMMENT '上一次登录ip',
  `status` varchar(255) DEFAULT NULL COMMENT '1正常 0删除',
  `create_time` bigint(20) DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(20) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of com_admin
-- ----------------------------
INSERT INTO `com_admin` VALUES ('1', 'admin', 'b653b24a61ccc1dc97f879cc4414c601', '512', 'lgy', null, null, null, '1', '1539826708', '1539826708');
