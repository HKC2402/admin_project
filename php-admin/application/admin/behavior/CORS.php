<?php
/**
 * Created by PhpStorm.
 * User: lgy
 * Date: 2018/10/30
 * Time: 11:23
 */

namespace app\admin\behavior;
use think\Response;

class CORS
{
    //跨域使用
    public function appInit(&$params)
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: token,Origin, X-Requested-With, Content-Type, Accept");
        header('Access-Control-Allow-Methods: POST,GET');
        if(request()->isOptions()){
            exit();
        }
    }
}