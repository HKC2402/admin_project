<?php
/**
 * Created by PhpStorm.
 * User: lgy
 * Date: 2018/9/12
 * Time: 21:25
 */

namespace app\admin\controller;
use think\Controller;
use think\Request;
use app\admin\model\Admin as AdminModel;
class Admin extends Base
{
	//添加管理员
	public function addAdmin(){
		$request = Request::instance();
		$params = $request->param();
		$res = AdminModel::addAdmin($params);
		if($res){
			return retSuccess('ok','添加成功');
		}else{
			return retError('error','添加失败');
		}
	}
	//获取管理员列表
	public function getAdminList(){
		$res = AdminModel::getAdminList();
		return retSuccess($res,'ok');
	}
	//获取管理员信息
	public function getAdminInfo(){
		$res = AdminModel::getAdminInfo();
		return json($res);
	}
    //删除管理员
    public function delAdminById(){
        $request = Request::instance();
        $params = $request->param();
        $res = AdminModel::delAdminById($params);
        if($res){
            return retSuccess('ok','删除成功');
        }else{
            return retError('error','删除失败');
        }
    }
}