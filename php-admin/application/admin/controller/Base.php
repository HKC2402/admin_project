<?php
/**
 * Created by PhpStorm.
 * User: lgy
 * Date: 2018/9/12
 * Time: 21:26
 */

namespace app\admin\controller;
use app\lib\exception\ParamsException;
use app\lib\exception\TokenException;
use think\Controller;
use app\admin\service\Token as TokenService;
use think\Request;

class Base extends Controller
{
	protected $beforeActionList = [
		'checkToken'  =>  ['except'=>'loginUser']
		//'checkToken'  =>  ['only'=>'方法名']
	];
	protected function checkToken(){
		$token = Request::instance()->header('token');
		if(!$token){
			throw new ParamsException([
				'msg'=>'token不能为空'
			]);
		}
		$result = TokenService::verifyToken($token);
		if(!$result){
			throw new TokenException([
				'errorCode'=>14444
			]);
		}
		return $result;
	}

}