<?php
/**
 * Created by PhpStorm.
 * User: lgy
 * Date: 2018/9/17
 * Time: 21:31
 */

namespace app\admin\controller;
use app\admin\service\WebToken;
use think\Request;

class Login extends Base
{
	//web版登陆
	public function loginUser(){
		$request = Request::instance();
		$params = $request->param();
		$user_token = new WebToken();
		$token['data']['token'] = $user_token->loginUser($params);
		$token['code'] = 1;
		$token['msg'] = '登录成功！';
		return json($token);
	}
	//web版退出
    public function loginOut(){
        $token = Request::instance()->header('token');
        cache($token,null);
        return retSuccess('ok','退出成功');
    }
}