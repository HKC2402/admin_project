<?php
/**
 * Created by PhpStorm.
 * User: lgy
 * Date: 2018/8/13
 * Time: 20:40
 */

namespace app\api\validate;

use app\lib\exception\ParamsException;
use think\Request;
use think\Validate;



class BaseValidate extends Validate
{
	public function goCheck(){
		//1.获取参数 2.做校验
		$request = Request::instance();
		$params = $request->param();
		$result = $this->check($params);
		if(!$result){
			$error = $this->error;
			throw new ParamsException([
				'msg'=>  $error
			]);
		}else{
			return true;
		}
	}
	//必须是正整数
	protected function isPositiveInteger($value,$rule='',$data='',$field=''){
		if(is_numeric($value) && is_int($value + 0 ) && ($value+0)>0){
			return true;
		}else{
			return false;
		}
	}
	//必须存在
	protected function isNotEmpty($value, $rule='', $data='', $field='')
	{
		if (empty($value)) {
			return false;
		} else {
			return true;
		}
	}
	//根据规则获取数据
	public function getDateByRule($arrays){
		if (array_key_exists('user_id',$arrays) | array_key_exists('uid',$arrays)){
			//不允许包含uid的参数进来
			throw new ParameterException([
				'msg'=>'参数包含非法user_id或是uid'
			]);
		}
		$newArray = [];
		foreach ($this->rule as $key=>$value){
			$newArray[$key] = $arrays[$key];
		}
		return $newArray;
	}

}