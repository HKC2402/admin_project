<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件
use think\Route;
Route::get('doc','reflection\Documents@run');

//处理返回成功结果函数
function retSuccess($data='ok',$msg="success",$code=1){
    $result['code']=$code;
    $result['msg']=$msg;
    $result['data']=$data;
    return json($result);
}
//处理返回错误结果函数
function retError($data='error',$msg="error",$code=0){
    $result['code']=$code;
    $result['msg']=$msg;
    $result['data']=$data;
    return json($result);
}
function curl_get($url, &$httpCode = 0){
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	//不做证书校验,部署在linux环境下请改为true
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
	$file_contents = curl_exec($ch);
	$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	curl_close($ch);
	return $file_contents;
}

function getRandChar($length){
	$str = null;
	$strPol = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
	$max = strlen($strPol) - 1;
	for ($i = 0;$i < $length;$i++) {
		$str .= $strPol[rand(0, $max)];
	}
	return $str;
}
//设置密码混淆
function psMD5($password){
	$password = $password.'dbkjlgy';
	return md5($password);
}
//七牛云获取token
function getQiniuTokenWx(){
	require_once APP_PATH . '/../vendor/qiniu/autoload.php';
	$accessKey = config('qiniu.ACCESSKEY');
	$secretKey = config('qiniu.SECRETKEY');
//        $auth = new Auth($accessKey, $secretKey);
	$auth = new Auth($accessKey, $secretKey);
	$bucket = config('qiniu.BUCKET');
	$upToken = $auth->uploadToken($bucket);
	$result['code']=1;
	$result['msg']='ok';
	$result['data'] = [
		'uptoken'=>$upToken,
	];
	return json($result);
}