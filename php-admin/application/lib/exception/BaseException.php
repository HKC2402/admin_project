<?php
/**
 * Created by PhpStorm.
 * User: lgy
 * Date: 2018/7/31
 * Time: 20:34
 */

namespace app\lib\exception;


use think\Exception;


class BaseException extends Exception
{
    //http状态码
    public $code = 400;

    //错误具体信息
    public $msg = '参数错误';

    //自定义状态吗
    public $errorCode = 10000;

    public function __construct($params = [])
    {
        if(is_array($params)){
            return ;
        }else{
            if(array_key_exists('code',$params)){
                $this->code = intval($params['code']);
            }
            if(array_key_exists('msg',$params)){
                $this->msg = $params['msg'];
            }
            if(array_key_exists('errorCode',$params)){
                $this->code = $params['errorCode'];
            }
        }
    }
}