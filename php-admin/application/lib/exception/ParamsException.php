<?php
/**
 * Created by PhpStorm.
 * User: lgy
 * Date: 2018/8/13
 * Time: 20:43
 */

namespace app\lib\exception;
class ParamsException extends BaseException
{
	public $code = 400;
	public $msg = "数据不对";
	public $errorCode = 10001;
}