class RandomNum{
    constructor(startNum,endNum,secNum,ranCon){
        this.startNum = startNum || 1;
        this.endNum = endNum || 36;
        this.secNum = secNum || 7;
        this.ranCon = ranCon || 888;
    }
    // 1.生成源数组 [1...36]
    makeBaseArr(){
        let baseArr = [];
        for(var i=this.startNum;i<=this.endNum;i++){
            baseArr.push(i);
        }
        return baseArr;
    }
    // 2.随机数据
    makeRandomArr(){
        let baseArr = this.makeBaseArr();
        let ran_arr = [];
        for(let i=0;i<this.ranCon;i++){
            ran_arr[i] = [];
            while(ran_arr[i].length < this.secNum){
                let num = this.sample(baseArr);
                if(ran_arr[i].indexOf(num) == -1){
                    ran_arr[i].push(num);
                }
            }
        }
        return ran_arr;
    }
    //3.统计相同数组
    countSameArr(isPosition){
        isPosition = isPosition || false;
        let ran_arr = this.makeRandomArr();
        let same_arr = [];
        for(let i=0;i<ran_arr.length;i++){
            let count = 0;
            same_arr[i] = {};
            for(let j=1;j<ran_arr.length;j++){
                if(isPosition){
                    if(ran_arr[i] == ran_arr[j]){
                        ran_arr.splice(j, 1);
                        count += 1;
                    }
                }else{
                    if(this.sumArray(ran_arr[i]) == this.sumArray(ran_arr[j])){
                        ran_arr.splice(j, 1);
                        count += 1;
                    }
                }
                
            }
            same_arr[i].name = ran_arr[i];
            same_arr[i].value = count;
        }
        return this.sortDesc(same_arr);
    }
    // 4.统计数字出现的频率
    countSameNum(isOrder){
        isOrder = isOrder || true;
        let ran_num = this.flatten(this.makeRandomArr());
        let baseArr  = this.makeBaseArr();
        let same_arr = [];
        for(let i=0;i<baseArr.length;i++){
            let count = 0;
            same_arr[i] = {};
            for(let j=0;j<ran_num.length;j++){
                if(baseArr[i] == ran_num[j]){
                    count += 1;
                }
            }
            same_arr[i].name = baseArr[i];
            same_arr[i].value = count;
        }
        if(isOrder){
            return this.sortDesc(same_arr)
        }else{
            return same_arr;
        }
        
    }
    //从数组中随机选出一个
    sample(arr){
        return arr[Math.floor(Math.random() * arr.length)];
    }
    // 数组中数字和
    sumArray(arr){
        if(Array.isArray(arr)){
            return arr.reduce((x,y)=> {return parseInt(x)+parseInt(y)});
        }else{
            console.log(arr);
            return 1
        }
        
    }
    //比较统计数组大小 倒叙
    sortDesc(arr){
        for(let j=0;j<arr.length-1;j++){
           //两两比较，如果后一个比前一个大，则交换位置。
           for(let i=0;i<arr.length-1-j;i++){
                if(arr[i].value < arr[i+1].value){
                    let temp = arr[i];
                    arr[i] = arr[i+1];
                    arr[i+1] = temp;
                }
            } 
        }
        return arr;
    }
    //合并数组
    flatten(arr){
        return arr.reduce((a, v) => a.concat(v), []);
    }
}
// export {RandomNum}

/**
 * RandomNum
 * startNum int 数据源范围开始数 比如36选择7 选取数范围为1-36 则填1
 * endNum int 数据源范围结束数 比如36选择7 选取数范围为1-36 则填36
 * secNum int 选择数 比如36选择7 则填7
 * ranCon int 循环次数 比如循环 8888 建议不要超过888888 否则程序卡死
 */
// var ranNum = new RandomNum(1,36,7,888);
// //重复数组出现次数统计
// console.log(ranNum.countSameArr());
// //重复数字出现次数统计
// console.log(ranNum.countSameNum());